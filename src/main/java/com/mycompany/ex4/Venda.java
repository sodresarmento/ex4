package com.mycompany.ex4;


public class Venda {
    
    
    private double precoUnitario;
    private double quantidadeVendida;
    

    public Venda() {
    }

    public Venda(double precoUnitario, double quantidadeVendida) {
        this.precoUnitario = precoUnitario;
        this.quantidadeVendida = quantidadeVendida;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public double getQuantidadeVendida() {
        return quantidadeVendida;
    }

    public void setQuantidadeVendida(double quantidadeVendida) {
        this.quantidadeVendida = quantidadeVendida;
    }

    
}
